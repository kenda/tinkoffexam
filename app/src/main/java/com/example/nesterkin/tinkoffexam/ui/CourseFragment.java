package com.example.nesterkin.tinkoffexam.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nesterkin.tinkoffexam.FintechApplication;
import com.example.nesterkin.tinkoffexam.R;
import com.example.nesterkin.tinkoffexam.data.FintechApi;
import com.example.nesterkin.tinkoffexam.data.model.homework.Homework;
import com.example.nesterkin.tinkoffexam.data.model.homework.Lecture;
import com.example.nesterkin.tinkoffexam.data.model.homework.Unit;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Nesterkin Alexander on 01/11/2018
 */
public class CourseFragment extends Fragment {

    @Inject
    FintechApi mFintechApi;

    private RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_course, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ((FintechApplication) getActivity().getApplication()).getComponent().inject(this);
        mRecyclerView = view.findViewById(R.id.recycler_view);

        mFintechApi.getHomeworks().enqueue(new Callback<Homework>() {
            @Override
            public void onResponse(Call<Homework> call, Response<Homework> response) {
                if (response.isSuccessful() && response.body() != null) {

                    List<Unit> tests = new ArrayList<>();

                    List<Lecture> lectures = response.body().getHomeworks();
                    for (Lecture lecture : lectures) {
                        List<Unit> units = lecture.getUnits();
                        for (Unit unit : units) {
                            if (unit.getTask().getTaskType().equals("test_during_lecture")) {
                                tests.add(unit);
                            }
                        }
                    }
                    UnitAdapter unitAdapter = new UnitAdapter(tests);
                    mRecyclerView.setAdapter(unitAdapter);
                }
            }

            @Override
            public void onFailure(Call<Homework> call, Throwable t) {

            }
        });
    }
}