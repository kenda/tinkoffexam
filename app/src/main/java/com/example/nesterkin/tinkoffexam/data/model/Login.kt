package com.example.nesterkin.tinkoffexam.data.model

/**
 * @author Nesterkin Alexander on 31/10/2018
 */
data class Login(val email: String,
                 val password: String)