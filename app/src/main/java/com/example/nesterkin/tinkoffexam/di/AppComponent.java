package com.example.nesterkin.tinkoffexam.di;

import com.example.nesterkin.tinkoffexam.ui.CourseFragment;
import com.example.nesterkin.tinkoffexam.ui.MainActivity;
import com.example.nesterkin.tinkoffexam.ui.ProfileFragment;
import com.example.nesterkin.tinkoffexam.ui.SettingsFragment;
import com.example.nesterkin.tinkoffexam.ui.SigninActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Nesterkin Alexander on 31/10/2018
 */
@Component(modules = AppModule.class)
@Singleton
public interface AppComponent {

    void inject(SigninActivity activity);

    void inject(MainActivity activity);

    void inject(CourseFragment fragment);

    void inject(ProfileFragment fragment);

    void inject(SettingsFragment fragment);
}