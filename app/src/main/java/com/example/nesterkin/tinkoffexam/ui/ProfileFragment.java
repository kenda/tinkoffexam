package com.example.nesterkin.tinkoffexam.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.nesterkin.tinkoffexam.FintechApplication;
import com.example.nesterkin.tinkoffexam.R;
import com.example.nesterkin.tinkoffexam.data.FintechApi;
import com.example.nesterkin.tinkoffexam.data.model.Profile;
import com.example.nesterkin.tinkoffexam.data.model.User;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.nesterkin.tinkoffexam.FintechApplication.BASE_API_URL;

/**
 * @author Nesterkin Alexander on 02/11/2018
 */
public class ProfileFragment extends Fragment {

    @Inject
    FintechApi mFintechApi;

    private ImageView mAvatarImageView;
    private RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ((FintechApplication) getActivity().getApplication()).getComponent().inject(this);

        mAvatarImageView = view.findViewById(R.id.avatar_image_view);
        mRecyclerView = view.findViewById(R.id.recycler_view);

        mFintechApi.getProfile().enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                if (response.isSuccessful() && response.body() != null) {

                    List<String> profileData = new ArrayList<>();

                    User user = response.body().getUser();

                    profileData.add(user.getBirthday());
                    profileData.add(user.getEmail());
                    profileData.add(user.getFirstName());
                    profileData.add(user.getLastName());
                    profileData.add(user.getRegion());
                    profileData.add(user.getUniversity());

                    if (user.getAvatar() != null) {
                        Glide.with(getActivity()).load(BASE_API_URL + user.getAvatar()).into(mAvatarImageView);
                    }

                    ProfileAdapter profileAdapter = new ProfileAdapter(profileData);
                    mRecyclerView.setAdapter(profileAdapter);
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {

            }
        });
    }
}