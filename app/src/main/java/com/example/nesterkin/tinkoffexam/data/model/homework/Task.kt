package com.example.nesterkin.tinkoffexam.data.model.homework

import com.google.gson.annotations.SerializedName

/**
 * @author Nesterkin Alexander on 01/11/2018
 */
data class Task(@SerializedName("id") val id: Int,
                @SerializedName("title") val title: String,
                @SerializedName("task_type") val taskType: String,
                @SerializedName("max_score") val maxScore: String,
                @SerializedName("deadline_date") val deadlineDate: String,
                @SerializedName("contest_info") val contestInfo: ContestInfo,
                @SerializedName("short_name") val shortName: String)