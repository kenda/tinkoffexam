package com.example.nesterkin.tinkoffexam.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.nesterkin.tinkoffexam.FintechApplication;
import com.example.nesterkin.tinkoffexam.R;

/**
 * @author Nesterkin Alexander on 01/11/2018
 */
public class MainActivity extends AppCompatActivity implements SettingsFragment.FragmentCallback {

    private BottomNavigationView mNavigationView;

    public static Intent newIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((FintechApplication) getApplication()).getComponent().inject(this);

        mNavigationView = findViewById(R.id.bottom_navigation_view);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.fragment_container, new ProfileFragment())
                .commit();

        if (savedInstanceState == null) {
            mNavigationView.setSelectedItemId(R.id.action_profile);
        }

        mNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.action_course:
                    fragmentManager.beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .replace(R.id.fragment_container, new CourseFragment())
                            .commit();
                    break;
                case R.id.action_profile:
                    fragmentManager.beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .replace(R.id.fragment_container, new ProfileFragment())
                            .commit();
                    break;
                case R.id.action_settings:
                    fragmentManager.beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .replace(R.id.fragment_container, new SettingsFragment())
                            .commit();
                    break;
            }

            return true;
        });
    }

    @Override
    public void callback() {
        startActivity(SigninActivity.newIntent(this));
    }
}