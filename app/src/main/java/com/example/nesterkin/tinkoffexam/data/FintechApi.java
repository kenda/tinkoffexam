package com.example.nesterkin.tinkoffexam.data;

import com.example.nesterkin.tinkoffexam.data.model.Login;
import com.example.nesterkin.tinkoffexam.data.model.Profile;
import com.example.nesterkin.tinkoffexam.data.model.User;
import com.example.nesterkin.tinkoffexam.data.model.homework.Homework;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * API к сервису fintech.tinkoff.ru
 *
 * @author Nesterkin Alexander on 31/10/2018
 */
public interface FintechApi {

    @POST("api/signin")
    Call<User> signIn(@Body Login login);

    @GET("api/course/android_fall2018/homeworks")
    Call<Homework> getHomeworks();

    @GET("api/user")
    Call<Profile> getProfile();

    @POST("api/signout")
    Call<String> signOut();
}