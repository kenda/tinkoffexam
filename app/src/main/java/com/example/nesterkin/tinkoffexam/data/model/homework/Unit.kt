package com.example.nesterkin.tinkoffexam.data.model.homework

/**
 * @author Nesterkin Alexander on 01/11/2018
 */
data class Unit(val id: Int,
                val task: Task,
                val status: String,
                val mark: String)