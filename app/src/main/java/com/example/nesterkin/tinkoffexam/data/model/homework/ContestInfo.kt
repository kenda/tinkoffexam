package com.example.nesterkin.tinkoffexam.data.model.homework

import com.google.gson.annotations.SerializedName

/**
 * @author Nesterkin Alexander on 01/11/2018
 */
data class ContestInfo(@SerializedName("contest_status") val contestStatus: ContestStatus,
                       @SerializedName("contest_url") val contestUrl: String)