package com.example.nesterkin.tinkoffexam.data.model.homework

import com.google.gson.annotations.SerializedName

/**
 * @author Nesterkin Alexander on 01/11/2018
 */
data class Homework(@SerializedName("homeworks") val homeworks: List<Lecture>)