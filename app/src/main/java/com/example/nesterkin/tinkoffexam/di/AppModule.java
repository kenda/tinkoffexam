package com.example.nesterkin.tinkoffexam.di;

import com.example.nesterkin.tinkoffexam.data.FintechApi;

import java.net.CookieManager;
import java.net.CookiePolicy;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.nesterkin.tinkoffexam.FintechApplication.BASE_API_URL;

/**
 * @author Nesterkin Alexander on 31/10/2018
 */
@Module
public class AppModule {

    @Provides
    CookieManager provideCookieManager() {
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        return cookieManager;
    }

    @Provides
    @Singleton
    FintechApi provideFintechApi(CookieManager cookieManager) {
        JavaNetCookieJar cookieJar = new JavaNetCookieJar(cookieManager);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .cookieJar(cookieJar)
                .addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(FintechApi.class);
    }
}