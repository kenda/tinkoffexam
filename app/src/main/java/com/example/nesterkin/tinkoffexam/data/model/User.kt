package com.example.nesterkin.tinkoffexam.data.model

import com.google.gson.annotations.SerializedName

/**
 * @author Nesterkin Alexander on 31/10/2018
 */
data class User(@SerializedName("birthday") val birthday: String?,
                @SerializedName("email") val email: String,
                @SerializedName("first_name") val firstName: String?,
                @SerializedName("last_name") val lastName: String?,
                @SerializedName("middle_name") val middleName: String?,
                @SerializedName("phone_mobile") val phoneMobile: String?,
                @SerializedName("t_shirt_size") val tShirtSize: String?,
                @SerializedName("is_client") val isClient: Boolean?,
                @SerializedName("skype_login") val skypeLogin: String?,
                @SerializedName("description") val description: String?,
                @SerializedName("region") val region: String?,
                @SerializedName("school") val school: String?,
                @SerializedName("school_graduation") val schoolGraduation: String?,
                @SerializedName("university") val university: String?,
                @SerializedName("faculty") val faculty: String?,
                @SerializedName("university_graduation") val universityGraduation: String?,
                @SerializedName("grade") val grade: String?,
                @SerializedName("department") val department: String?,
                @SerializedName("current_work") val currentWork: String?,
                @SerializedName("resume") val resume: String?,
                @SerializedName("notifications") val notifications: List<String>?,
                @SerializedName("id") val id: Int,
                @SerializedName("admin") val admin: Boolean?,
                @SerializedName("avatar") val avatar: String?)