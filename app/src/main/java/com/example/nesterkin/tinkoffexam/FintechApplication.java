package com.example.nesterkin.tinkoffexam;

import android.app.Application;

import com.example.nesterkin.tinkoffexam.di.AppComponent;
import com.example.nesterkin.tinkoffexam.di.DaggerAppComponent;

/**
 * @author Nesterkin Alexander on 31/10/2018
 */
public class FintechApplication extends Application {

    public static final String BASE_API_URL = "https://fintech.tinkoff.ru";

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder()
                .build();
    }

    public AppComponent getComponent() {
        return mAppComponent;
    }
}