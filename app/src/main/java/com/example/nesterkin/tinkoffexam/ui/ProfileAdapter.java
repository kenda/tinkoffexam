package com.example.nesterkin.tinkoffexam.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nesterkin.tinkoffexam.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nesterkin Alexander on 02/11/2018
 */
public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ProfileViewHolder> {

    private final List<String> mProfileData;

    ProfileAdapter(List<String> profileData) {
        mProfileData = new ArrayList<>(profileData);
    }

    @NonNull
    @Override
    public ProfileViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_view_unit, viewGroup, false);
        return new ProfileViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileViewHolder profileViewHolder, int i) {
        profileViewHolder.bind(mProfileData.get(i));
    }

    @Override
    public int getItemCount() {
        return mProfileData.size();
    }

    class ProfileViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitleTextView;

        public ProfileViewHolder(@NonNull View itemView) {
            super(itemView);
            mTitleTextView = itemView.findViewById(R.id.unit_title);
        }

        void bind(String data) {
            mTitleTextView.setText(data);
        }
    }
}