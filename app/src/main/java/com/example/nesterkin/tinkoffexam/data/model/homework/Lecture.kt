package com.example.nesterkin.tinkoffexam.data.model.homework

import com.google.gson.annotations.SerializedName

/**
 * @author Nesterkin Alexander on 01/11/2018
 */
data class Lecture(@SerializedName("id") val id: Int,
                   @SerializedName("title") val title: String,
                   @SerializedName("tasks") val units: List<Unit>)