package com.example.nesterkin.tinkoffexam.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.nesterkin.tinkoffexam.FintechApplication;
import com.example.nesterkin.tinkoffexam.R;
import com.example.nesterkin.tinkoffexam.data.FintechApi;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Nesterkin Alexander on 02/11/2018
 */
public class SettingsFragment extends Fragment {

    @Inject
    FintechApi mFintechApi;

    private FragmentCallback mFragmentCallback;
    private Button mSignoutButton;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof FragmentCallback) {
            mFragmentCallback = (FragmentCallback) getActivity();
        } else {
            throw new RuntimeException("Activity should implement FragmentCallback");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ((FintechApplication) getActivity().getApplication()).getComponent().inject(this);
        mSignoutButton = view.findViewById(R.id.signout_button);
        mSignoutButton.setOnClickListener(__ -> logout());
    }

    private void logout() {
        mFintechApi.signOut().enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                mFragmentCallback.callback();
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

            }
        });
    }

    public interface FragmentCallback {

        void callback();
    }
}