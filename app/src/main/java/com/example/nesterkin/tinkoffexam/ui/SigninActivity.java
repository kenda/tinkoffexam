package com.example.nesterkin.tinkoffexam.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nesterkin.tinkoffexam.FintechApplication;
import com.example.nesterkin.tinkoffexam.R;
import com.example.nesterkin.tinkoffexam.data.FintechApi;
import com.example.nesterkin.tinkoffexam.data.model.Login;
import com.example.nesterkin.tinkoffexam.data.model.User;

import java.io.IOException;
import java.net.CookieManager;
import java.net.UnknownHostException;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SigninActivity extends AppCompatActivity {

    @Inject
    FintechApi mFintechApi;
    @Inject
    CookieManager mCookieManager;

    private EditText mLoginEditText;
    private EditText mPasswordEditText;
    private TextView mResultTextView;
    private Button mSigninButton;

    public static Intent newIntent(Context context) {
        return new Intent(context, SigninActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ((FintechApplication) getApplication()).getComponent().inject(this);

        mLoginEditText = findViewById(R.id.login_edit_text);
        mPasswordEditText = findViewById(R.id.password_edit_text);
        mResultTextView = findViewById(R.id.result);
        mSigninButton = findViewById(R.id.signin_button);

        mSigninButton.setOnClickListener(view -> signinRequest());
    }

    private void signinRequest() {
        Login login = new Login(mLoginEditText.getText().toString(), mPasswordEditText.getText().toString());
        mFintechApi.signIn(login).enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                if (response.isSuccessful() && response.body() != null) {
                    mResultTextView.setText(response.body().toString());
                    startMainActivity();
                }
                if (response.code() == 403 && response.errorBody() != null) {
                    try {
                        mResultTextView.setText(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable throwable) {
                startMainActivity();
                if (throwable instanceof UnknownHostException) {
                    mResultTextView.setText("Проверьте интернет" + "\n" + throwable.getMessage());
                }
            }
        });
    }

    private void startMainActivity() {
        mCookieManager.getCookieStore().removeAll();
        startActivity(MainActivity.newIntent(this));
    }
}