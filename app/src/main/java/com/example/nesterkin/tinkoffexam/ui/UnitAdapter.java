package com.example.nesterkin.tinkoffexam.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nesterkin.tinkoffexam.R;
import com.example.nesterkin.tinkoffexam.data.model.homework.Unit;

import java.util.List;

/**
 * @author Nesterkin Alexander on 01/11/2018
 */
public class UnitAdapter extends RecyclerView.Adapter<UnitAdapter.UnitViewHolder> {

    private List<Unit> mTasks;

    UnitAdapter(List<Unit> tasks) {
        mTasks = tasks;
    }

    @NonNull
    @Override
    public UnitViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_view_unit, viewGroup, false);
        return new UnitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UnitViewHolder testViewHolder, int i) {
        testViewHolder.bind(mTasks.get(i));
    }

    @Override
    public int getItemCount() {
        return mTasks.size();
    }

    static class UnitViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitleTextView;
        private TextView mMarkTextView;

        public UnitViewHolder(@NonNull View itemView) {
            super(itemView);
            mTitleTextView = itemView.findViewById(R.id.unit_title);
            mMarkTextView = itemView.findViewById(R.id.unit_mark);
        }

        void bind(Unit unit) {
            mTitleTextView.setText(unit.getTask().getTitle());
            mMarkTextView.setText(unit.getMark());
        }
    }
}