package com.example.nesterkin.tinkoffexam.data.model

/**
 * @author Nesterkin Alexander on 02/11/2018
 */
data class Profile(val user: User,
                   val status: String)